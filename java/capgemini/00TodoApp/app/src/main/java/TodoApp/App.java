/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package TodoApp;

import controller.ProjectController;
import controller.TaskController;
import java.util.Date;
import java.util.List;
import model.Project;
import model.Task;

public class App {
    public static void main(String[] args) {
        
        ProjectController projectController = new ProjectController();

        Project project = new Project();
//        project.setName("Projeto teste");
//        project.setDescription("Descrição");
//        projectController.save(project);

        project.setId(1);
        project.setName("Projeto teste - Atualizado");
        project.setDescription("Descrição");
        projectController.update(project);

        List<Project> projects = projectController.getAll();
        System.out.println("Total de projetos = " + projects.size());

//        projectController.removeById(1);

        TaskController taskController = new TaskController();

        Task task = new Task();
        task.setIdProject(1);
        task.setName("Criar as telas do aplicativo");
        task.setDescription("Devem ser criadas telas para os cadastros");
        task.setNotes("Sem notas");
        task.setCompleted(false);
        task.setDeadline(new Date());

        taskController.save(task);

        task.setName("Alterar as telas do aplicativo");
        taskController.update(task);
        List<Task> tasks = taskController.getAll(task.getIdProject());
        System.out.println("Total de tarefas = " + tasks.size());
    }
}
